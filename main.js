"use strict";

const btns = document.querySelectorAll(".btn");

document.addEventListener("keydown", (event) => {
  let keyPressed = event.code;

  if (event.code.startsWith("Key")) keyPressed = keyPressed.substring(3);

  btns.forEach((btn) => {
    keyPressed === btn.innerText
      ? btn.classList.add("active")
      : btn.classList.remove("active");
  });
});
